function p = legendrePoly(n)
% LEGENDREPOLY 计算n阶勒让德多项式，输出符号变量表示的n阶勒让德多项式
% MATLAB内置函数：连带勒让德函数 legendre();
%                Symbolic Math Toolbox 中的勒让德多项式函数 LegendrelegendreP().

syms x
f = ( x * x - 1)^n;    % 计算勒让德多项式中的x^2-1
y = diff( f , n);      % 对f多项式进行n阶求导
str = prod(1:n);     % 求n的阶乘
l = (1 / (2^n * str))*y;    % 勒让德多项式的一般表示式
p = collect(l);   % 对求出的勒让德多项式进行合并同类项

end

