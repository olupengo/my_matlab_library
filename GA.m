function ga = GA(alpha,phi,omega,t,expansion)
% GA 发射惯性坐标系到发射坐标系的方向余弦矩阵。
% 发射坐标系：坐标原点O在发射点，y轴过发射点，垂直于发射点所在水平面，指向上为正。x轴在
%    水平面上指向发射瞄准方向。z轴与x轴和y轴构成右手直角坐标系。该坐标系随着地球一起旋转。
% 发射惯性坐标系：发射惯性坐标系在发射瞬时和发射坐标系重合，之后发射惯性坐标系相对于惯性
%    空间静止。
% 使用时要注意坐标系定义和旋转顺序！
% 参数：
%   alpha   地心方位角或发射方位角，单位：rad
%   phi     如果alpha使用地心方位角，phi必须使用地心维度；如果alpha使用发射方位角，phi必须使用地理维度，单位：rad
%   omega   地球自转角速度，标量，单位：rad/s
%   t       发射时间，单位：s
%   expansion  展开精度，默认不展开，是可选参数
%                 当expansion=1时，将正弦和余弦函数展开成omega*t的幂级数，忽略1阶及1阶以上的项
%                 当expansion=2时，将正弦和余弦函数展开成omega*t的幂级数，忽略2阶及2阶以上的项
% 输出：
%   ga  发射惯性坐标系到发射坐标系的方向余弦矩阵
% 
% 参考：
%   [1] 王志刚. 远程火箭与卫星轨道力学基础[M]. 西北工业大学出版社, 2006.
% 
% 鲁鹏
% 2019.08.22

if nargin < 5
    expansion = 0;
end

switch expansion
    case 0
        g11 = cos(alpha)^2 * cos(phi)^2 * (1-cos(omega*t)) + cos(omega*t);
        g12 = cos(alpha)*sin(phi)*cos(phi)*(1-cos(omega*t)) - sin(alpha)*cos(phi)*sin(omega*t);
        g13 = -sin(alpha)*cos(alpha)*cos(phi)^2 * (1-cos(omega*t)) - sin(phi)*sin(omega*t);
        g21 = cos(alpha)*sin(phi)*cos(phi)*(1-cos(omega*t)) + sin(alpha)*cos(phi)*sin(omega*t);
        g22 = sin(phi)^2 * (1-cos(omega*t)) + cos(omega*t);
        g23 = -sin(alpha)*sin(phi)*cos(phi) * (1-cos(omega*t)) + cos(alpha)*cos(phi)*sin(omega*t);
        g31 = -sin(alpha)*cos(alpha)*cos(phi)^2 * (1-cos(omega*t)) + sin(phi)*sin(omega*t);
        g32 = -sin(alpha)*sin(phi)*cos(phi) * (1-cos(omega*t)) - cos(alpha)*cos(phi)*sin(omega*t);
        g33 = sin(alpha)^2 * cos(phi)^2 * (1-cos(omega*t)) + cos(omega*t);
    case 1
        omegax = omega * cos(phi) * cos(alpha);
        omegay = omega * sin(phi);
        omegaz = -omega * cos(phi) * sin(alpha);
        
        g11 =       1     ;
        g12 =   omegaz * t;
        g13 = - omegay * t;
        g21 = - omegaz * t;
        g22 =       1     ;
        g23 =   omegax * t;
        g31 =   omegay * t;
        g32 = - omegax * t;
        g33 =       1     ;
    case 2
        omegax = omega * cos(phi) * cos(alpha);
        omegay = omega * sin(phi);
        omegaz = -omega * cos(phi) * sin(alpha);
        
        g11 = 1 - (omega^2 - omegax^2) * t^2 / 2;
        g12 = omegaz*t + omegax*omegay*t^2 / 2;
        g13 = -omegay*t + omegax*omegaz*t^2 / 2;
        g21 = -omegaz*t + omegax*omegay*t^2 / 2;
        g22 = 1 - (omega^2 - omegay^2) * t^2 / 2;
        g23 = omegax*t + omegay*omegaz*t^2 / 2;
        g31 = omegay*t + omegax*omegaz*t^2 / 2;
        g32 = -omegax*t + omegay*omegaz*t^2 / 2;
        g33 = 1 - (omega^2 - omegaz^2) * t^2 / 2;
end

ga = [g11, g12, g13;
    g21, g22, g23;
    g31, g32, g33];

end

