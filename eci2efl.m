function v_efl = eci2efl(vec, A, B0, B1, time)
%ECI2EFL 地心惯性坐标系坐标转换到发射坐标系的函数
% 输入 
%   vec：待转化向量，3xN
%   A：发射方位角，rad
%   B0：发射时刻发射场赤经，rad
%   B1：发射场的地理维度，rad
%   time: 距离发射时刻(time=0)的时间

omega = 7.29211585e-5;
M = My(-A-pi/2)*Mx(B1)*Mz(omega*time-pi/2+B0);
v_efl = M*vec;

end

function mx = Mx(alpha)
% 绕x轴旋转的alpha弧度的坐标转换矩阵
mx = [1,      0     ,     0     ;
      0,  cos(alpha), sin(alpha);
      0, -sin(alpha), cos(alpha)];
end
function my = My(alpha)
% 绕y轴旋转的alpha弧度的坐标转换矩阵
my = [cos(alpha), 0 ,-sin(alpha);
           0    , 1 ,     0     ;
      sin(alpha), 0 , cos(alpha)];
end
function mz = Mz(alpha)
% 绕z轴旋转的alpha弧度的坐标转换矩阵
mz = [ cos(alpha), sin(alpha), 0;
      -sin(alpha), cos(alpha), 0;
           0     ,     0,      1];
end
