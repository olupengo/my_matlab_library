function plot_orbit(orbit_element, rocket_position)
% 根据轨道六根数绘制轨道和卫星位置
% Input：
%   orbit_element [a半长轴,e偏心率,i轨道倾角,Omega升交点赤经,omega近地点幅角,nu入轨点真近点角]
%   rocket_position 火箭上升轨道 [r1';r2';...;rn']
% Output:
%   地球和卫星轨道
% example:
% 
% 2021.07.13

R = 6.371E6;   % 地球平均半径，单位：m
% 肯尼迪航天中心维度（28.6°N,80.7°W），西昌卫星发射中心（28.2°N,102°E），
% 酒泉发射场（40.7525263°N,99.9966019°E），文昌卫星发射中心（19.63°N,110.956°E），
% 太原卫星发射中心(38.71°N,111.96°E)
B0 = 19.63*pi/180;   % 发射点地理维度，单位：rad
B1 = 110.956*pi/180;     % 发射场地理经度
alphaE = 1/298.257;  % 地球扁率
mu0 = alphaE*sin(2*B0);     % 重力方向与地心矢径的夹角
phi0 = atan(tan(B0)*(1-alphaE)^2);   % 发射点的地心维度，单位：rad

a = orbit_element(1);  % 单位：R
e = orbit_element(2);               % 圆轨道
i = orbit_element(3);
Omega = orbit_element(4);
omega = orbit_element(5);
nu = orbit_element(6);

A0 = asin(cos(i)/cos(phi0)); % 求发射方位角，单位：rad
Delta_lambda = -asin(tan(phi0)/tan(i));   % 升交点与发射点经度差(发射点-升交点), 注意区分升轨发射和降轨发射
Rl = [-R*sin(mu0)*cos(A0);
       R*cos(mu0);
       R*sin(mu0)*sin(A0)];  % 发射点地心矢径在发射惯性坐标系中的分量

Omega_g = Omega + pi + Delta_lambda - B1;  % 格林尼治天文台的赤经
C = Mz(omega)*Mx(i)*Mz(Omega);      % x轴指向春分点的地心惯性坐标系到地心轨道坐标系(x轴指向近地点，y轴指向运动方向)的转换矩阵
% Co = My(i)*Mz(-Delta_lambda)*My(-pi/2)*Mz(B0)*My(A0);  % 发射惯性坐标系到升交点轨道坐标系转换矩阵
% Ca = Mz(-(omega + f))*Co;   % 发射惯性坐标系到入轨点轨道坐标系转换矩阵
theta = -pi:pi/50:pi;
n = length(theta);
theta = theta';
norm_r = a * (1 - e^2) ./ (1 + e * cos(theta));  %
r = [norm_r.*cos(theta), norm_r.*sin(theta), zeros(n,1)];  % 地心轨道坐标系下的坐标
for k = 1:n
    r(k,:) = (C\r(k,:)');   % x轴指向春分点的地心惯性坐标系下的坐标
end
% 绘制地球
plot_texture_globe(Omega_g/pi*180)
hold on
% 绘制卫星运行轨道
plot3(r(:,1), r(:,2), r(:,3), 'Linewidth',1.333)
xlabel('x')
ylabel('y')
zlabel('z')

% 绘制近地点位置
norm_r_Perigee = a * (1 - e^2) ./ (1 + e * cos(0));  % 卫星距离地心距离
r_Perigee = [norm_r_Perigee*cos(0), norm_r_Perigee*sin(0), 0];  % 卫星在地心轨道坐标系下的坐标
r_Perigee = C\r_Perigee';   % 卫星在x轴指向春分点的地心惯性坐标系下的坐标
scatter3(r_Perigee(1), r_Perigee(2), r_Perigee(3), 'filled')
text(r_Perigee(1), r_Perigee(2), r_Perigee(3), '\leftarrow 近地点')

% 绘制远地点地点位置
norm_r_Apogee = a * (1 - e^2) ./ (1 + e * cos(pi));  % 卫星距离地心距离
r_Apogee = [norm_r_Apogee*cos(pi), norm_r_Apogee*sin(pi), 0];  % 卫星在地心轨道坐标系下的坐标
r_Apogee = C\r_Apogee';   % 卫星在x轴指向春分点的地心惯性坐标系下的坐标
scatter3(r_Apogee(1), r_Apogee(2), r_Apogee(3), 'filled')
text(r_Apogee(1), r_Apogee(2), r_Apogee(3), '\leftarrow 远地点')

% 绘制入轨点位置
norm_r_Sat = a * (1 - e^2) ./ (1 + e * cos(nu));     % 入轨点距离地心距离
r_Sat = [norm_r_Sat*cos(nu), norm_r_Sat*sin(nu), 0];  % 入轨点在地心轨道坐标系下的坐标
r_Sat = C\r_Sat';   % 入轨点在x轴指向春分点的地心惯性坐标系下的坐标
scatter3(r_Sat(1), r_Sat(2), r_Sat(3), 'filled')
text(r_Sat(1), r_Sat(2), r_Sat(3), '\leftarrow 入轨点')

% 绘制发射场位置
longitude = B1;
latitude = B0;
r_launch = [cos(longitude)*cos(latitude);
            sin(longitude)*cos(latitude);
            sin(latitude)];  % 地球固连坐标系中的坐标
r_la = Rz(Omega_g)*r_launch;
scatter3(r_la(1),r_la(2),r_la(3),...
    'o','MarkerEdgeColor','r','MarkerFaceColor','r')
text(r_la(1), r_la(2), r_la(3), '\leftarrow 发射场')

% 绘制火箭轨迹
r_rocket = rocket_position;
plot3(r_rocket(:,1), r_rocket(:,2),r_rocket(:,3),'b')

end

function mx = Mx(alpha)
% 绕x轴旋转的alpha弧度的坐标转换矩阵
mx = [1,      0     ,     0     ;
      0,  cos(alpha), sin(alpha);
      0, -sin(alpha), cos(alpha)];
end
function my = My(alpha)
% 绕y轴旋转的alpha弧度的坐标转换矩阵
my = [cos(alpha), 0 ,-sin(alpha);
           0    , 1 ,     0     ;
      sin(alpha), 0 , cos(alpha)];
end
function mz = Mz(alpha)
% 绕z轴旋转的alpha弧度的坐标转换矩阵
mz = [ cos(alpha), sin(alpha), 0;
      -sin(alpha), cos(alpha), 0;
           0     ,     0,      1];
end
function rz = Rz(alpha)
% 绕z轴旋转的alpha弧度的旋转矩阵
rz = [cos(alpha),-sin(alpha), 0;
      sin(alpha), cos(alpha), 0;
          0     ,     0,      1];
end