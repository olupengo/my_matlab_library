function h = plot_texture_globe(Omega)
%PLOT_TEXTURE_GLOBAL 绘制贴图的地球
%
% 输入：Omega 格林尼治天文台的赤经，单位 deg
%
% 鲁 鹏
% 2021.07.13

% 设置默认格林尼治天文台的赤经为0
if nargin == 0
    Omega = 0;
end

% 绘制地心惯性坐标系
d = 1.3;  % 坐标轴查长度
quiver3(0,0,0,d,0,0, 'Color','r','LineWidth',1.5); text(d,0,0, 'X')
hold on;
quiver3(0,0,0,0,d,0, 'Color','r','LineWidth',1.5); text(0,d,0, 'Y')
quiver3(0,0,0,0,0,d, 'Color','r','LineWidth',1.5); text(0,0,d, 'Z')

earth_texture = imread('2_earth_no_clouds_2k.jpg');
% imshow(earth_texture);
earth_texture = flipud(earth_texture);
% imshow(earth_texture);

[x, y, z]=sphere(40);
h = surface(x, y, z);
set(h, 'CData', earth_texture, 'FaceColor', 'texturemap', ...
    'EdgeColor', 'none');%texturemap纹理贴图
axis equal
set(gca,'CameraViewAngleMode','manual');
view(-187, 26);

% 将地球旋转 Omega
direction = [0 0 1];  % 地心指向北极的单位矢量
rotate(h, direction, Omega, [0 0 0])
axis off

end

