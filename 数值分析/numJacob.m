% numerical Jacobians
% This J function works with any function f. *Note* that x and the result 
% of f must be in a vertical vector for it to work.
% 
% by Matthieu Heitz on 2 Feb 2017
% I get this script on website https://ww2.mathworks.cn/matlabcentral/
%     answers/28066-numerical-jacobian-in-matlab
% 
% modified by Peng Lu from Beijing Institute of Technology on 25 Apr 2019
% the original program had fatial errors

% 
% % Jacobian functor
% J = @(x,h,F)(F(repmat(x,size(x'))+diag(h))-F(repmat(x,size(x'))))./h';
% Your function
f = @(x)[x(1)^2 + x(2)^2; x(1)^3.*x(2)^3; x(3)^2];
% Point at which to estimate it
x = [1;1;1];
% Step to take on each dimension (has to be small enough for precision)
h = 1e-5*ones(size(x));
% Compute the jacobian
Jacobi(x,h,f)

% Jacobian functor
function J = Jacobi(x, h, F)
    n = length(x);
    xh = repmat(x, size(x')) + diag(h);
    J = nan(n, n);
    for i = 1:n
        J(:,i) = (F(xh(:,i)) - F(x)) ./ h;
    end
end