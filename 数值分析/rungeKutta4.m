% RUNGEKUTTA4 4阶龙格库塔法
% 作   者： hyowinner ( MATALB中文论坛用户 )
% 创建日期： 未 知
% 修 改 人： 鲁 鹏 ( 北京理工大学宇航学院 )
% 修改日期： 2018 年 6月 24 日
% 参考网站： http:// www.ilovematlab.cn/thread−42808−1−1.html
% 版   本： 2.0

function [ x , y ] = rungeKutta4( ufunc , a , b , y0 , h )
% 使用4阶龙格库塔法计算数值积分（参数形式参考了ode45函数）
% Input：
%   ufunc   被积函数
%   a       积分下限
%   b       积分上限
%   y0      积分初值
%   h       积分步长
% Output：
%   x   积分变量的值
%   y   与x对应的积分结果

n = floor( ( b - a ) / h );   % 求步数
x = ones (n, 1);
x(1) = a;       % 时间起点
y(:, 1) = y0;   % 赋初值，可以是向量，但是要注意维数
for ii = 1 : n
    x(ii + 1) = x(ii) + h;
    k1 = ufunc( x(ii), y(:, ii) );
    k2 = ufunc( x(ii) + h / 2 , y(:, ii) + h * k1 / 2 );
    k3 = ufunc( x(ii) + h / 2 , y(:, ii) + h * k2 / 2 );
    k4 = ufunc( x(ii) + h , y(:, ii) + h * k3 );
    y(:, ii +1) = y(:, ii) + h * ( k1 + 2 * k2 + 2 * k3 + k4 ) / 6;
end
y = y';

end