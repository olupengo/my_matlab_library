% modified newton method 很多书翻译为牛顿下山法
% 用牛顿下山法求解一下非线性方程的根
% x^3 - x - 1 = 0
% 
% references
% [1] https://www.docin.com/p-1025891968.html
% [2] https://blog.csdn.net/zhaoyl03/article/details/8228732


%% 本程序是根据参考网站2的程序改的
x0 = 0.6;
error1 = 1e-4;
error2 = 1e-4;
func = @(x) x^3-x-1;
dfunc = @(x) 3*x^2-1;

k = 1;
f0 = feval(func,x0);
x1 = x0-f0/feval(dfunc,x0);   %此时sigma=1，可以不加sigma
f1 = feval(func,x1);

while abs(x1-x0)>error1 && abs(f1)>error2
    sigma = 1;
    while abs(f1)>=abs(f0)
        sigma=sigma/2;
        x1=x0-sigma*(f0/feval(dfunc,x0));
        f1 = feval(func,x1);
    end % 内层While满足附加条件，以保证单调性
    k=k+1;
    x0=x1;
    f0 = f1;
    x1=x0-f0/feval(dfunc,x0);
    f1 = feval(func,x1);
end % 外层while向前走一步

%% 求解以下非线性方程组
% x1 + 2*x2 - 3 = 0
% 2*x1^2 + x2^2 - 5 = 0
F = @(x0)[x0(1) + 2*x0(2) - 3;
          2*x0(1)^2 + x0(2)^2 - 5];% F = [f1(x1,x2);f2(x1,x2)] = [0;0]
dF = @(x0)[1 2;
           4*x0(1) 2*x0(2)];% dF是F的雅克比矩阵
x0 = [-2;10]; %解的初始猜测值或估计值
error1 = 1e-4;
error2 = 1e-4;
% tol = 0.000005; %设置容许的误差

x1 = x0 - dF(x0)\F(x0);
k = 0;
while max(abs(x1-x0))>error1 && max(abs(F(x1)))>error2
    sigma = 1;
    k = k + 1;
    while F(x1)'*F(x1) >= F(x0)'*F(x0)
        sigma=sigma/2;
        x1 = x0 - sigma*dF(x0)\F(x0);
    end
    x0=x1;
    x1 = x0 - dF(x0)\F(x0);
end