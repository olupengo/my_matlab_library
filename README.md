# 我的MATLAB程序库

#### 介绍
自己用MATLAB写的常用子程序，方便日后使用。

#### 注意
本仓库中的MATLAB脚本使用的编码格式都是UTF-8，Windows下的MATLAB打开这些文件会出现乱码。Windows下的MATLAB创建的脚本默认的编码格式是GBK，Linux中的MATLAB创建的脚本默认的编码格式是UTF-8。如果你使用Windows下的MATLAB，提前对文件进行转码是一个不错的主意。或者，你也可以改变Windows版MATLAB的配置文件，将其默认编码格式改为UTF-8。